<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\TriDechets;

class WasteController extends AbstractController
{
    /**
     * @Route("/", name="waste")
     */

    public function index()
    {

        $Dechets = new TriDechets();
        $Dechets->lectureFichierJson();
        $Dechets->AfficherDechets();
        $Dechets->afficherServices();



        return $this->render('waste/index.html.twig', [
        ]);
    }
}
